import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)

const store = new Vuex.Store({
  strict:true, //使用严格模式
  state: {
    pageLoading: true, //页面是否在加载中,
    userID:'',//用户id
    // userID:'869115037298615,869115037349111',//用户id
    isNetworkOut: false, // 网络是否断开，默认未断开
    mid: undefined, // 下单支付时的大师id
    openWebView: false,// 是否打开webview 默认关闭状态
    routerPath: undefined,// 进入首页时上次地址的路由
  },

  mutations: {
    //显示或者隐藏加载页面
    changePageLoading (state,status) {
      state.pageLoading = status;
    },
    // 修改network的状态值
    changeNetworkStatus (state,status){
      state.isNetworkOut = status;
    },
    //设置uid
    setUserID (state,date){
      state.userID = date
    },
    // 设置下单时的大师id　mid
    setMid(state, mid) {
      state.mid = mid
    },
    // 设置打开webview的状态
    setWebView(state, status) {
      state.openWebView = status
    },
    // 设置进入首页时的路由
    setRouterPath(state, status) {
      state.routerPath = status
    }
  },
  actions:{
    setUserID (context){
      document.addEventListener( "plusready", function(){
        plus.device.getInfo({
          success:function(data){
            context.commit('setUserID',data.uuid)
          },
          fail:function(e){
            console.error('获取用户ID失败')
          }
        })
      }, false );
    }
  }
})

export default store;