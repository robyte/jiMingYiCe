/**
 * @function 判断应用升级模块，每7天进行自动检测或者用户点击检查更新时进行检测
 * @param context  this指向
 * @param w Object window对象
 * @param isUserCheck Boolean 是否为用户点击的更新检查
 * @author lf
 */
// 引入urlConfig 路径配置文件
const urlConfig = require("./url-config")
const serverUrl = urlConfig.fetchPrefix + '/app/Jmyc/get_version' //获取升级描述文件服务器地址
function updateVersion(context, w, isUserCheck = false) {
  plus.storage.clear()
  var downloadUrl = undefined, // 下载地址
    curVer = null,// 当前应用版本号
    srvVer = null, // 服务器上版本号
    keyUpdate = "updateCheck",//取消升级键名
    checkInterval = 604800000;//升级检查间隔，单位为ms,7天为7*24*60*60*1000=604800000, 如果每次启动需要检查设置值为0

  /**
   * 准备升级操作
   * 创建升级文件保存目录
   */
  function initUpdate() {
    // 在流应用模式下不需要检测升级操作
    if (navigator.userAgent.indexOf('StreamApp') >= 0) {
      return;
    }
    // 获取当前app的应用版本号
    plus.runtime.getProperty(plus.runtime.appid, function success(appInfo) {
      curVer = appInfo.version
      // 检测更新
      checkUpdate()
    })
  }

  /**
   * 检测程序升级
   */
  function checkUpdate() {
    // 如果是用户点击的检查更新，则直接进行判断
    if (isUserCheck) {
      getUpdateData()
      return
    }
    // 判断升级检测是否过期
    var lastcheck = plus.storage.getItem(keyUpdate);
    if (lastcheck) {
      var dc = parseInt(lastcheck);
      // 当前时间戳
      var dn = (new Date()).getTime();
      if (dn - dc < checkInterval) {	// 未超过上次升级检测间隔，不需要进行升级检查
        return;
      }
      // 取消已过期，删除取消标记
      plus.storage.removeItem(keyUpdate);
    }
    // 失败表示文件不存在，从服务器获取升级数据
    getUpdateData();
  }

  /**
   * 检查升级数据
   */
  function checkUpdateData() {
    // 判断是否需要升级
    if (compareVersion(curVer, srvVer)) {
      // 提示用户是否升级
      context.$vux.confirm.show({
        title: "版本更新",
        content: `发现新版本V${srvVer}，是否进行更新`,
        confirmText: "立即更新",
        cancelText: "取消",
        theme: "android",
        hideOnBlur: true,
        onCancel: () => {
          // 设置点击取消升级的时间
          plus.storage.setItem(keyUpdate, (new Date()).getTime().toString());
        },
        onConfirm: () => {
          // 从服务器下载apk文件
          createDownload();
          context.$vux.toast.text('下载中...', 'middle')
        }
      })
    } else {
      if (isUserCheck) {
        context.$vux.alert.show({
          title: '温馨提示',
          content: '当前已是最新版本，暂无更新！',
          hideOnBlur: true,
        })
      }
      return
    }

  }

  /**
   * 从服务器获取升级数据
   */
  function getUpdateData() {
    // 获取服务器上的版本号和下载地址
    context.$fetch.post(serverUrl, {}).then(res => {
      if (res.code == 200) {
        downloadUrl = res.data.download
        srvVer = res.data.version
        // 监听应用启动界面关闭事件
        if (plus.navigator.hasSplashscreen()) {
          document.addEventListener('splashclosed', checkUpdateData, false)
        } else {// 启动界面已经关闭
          checkUpdateData()
        }

      } else {
        context.$vux.toast.text('检查更新失败，请稍后重试', 'middle')
        return
      }
    }).catch(() => {
      context.$vux.toast.text('检查更新失败，请稍后重试', 'middle')
      return
    })
  }

  /**
   * 比较版本大小，如果新版本nv大于旧版本ov则返回true，否则返回false
   * @param {String} ov
   * @param {String} nv
   * @return {Boolean}
   */
  // function compareVersion(ov, nv) {
  //   if (nv != ov) {
  //     return true
  //   }
  //   return false
  // }
  function compareVersion(ov, nv) {
    if (!ov || !nv || ov == "" || nv == "") {
      return false;
    }
    var b = false,
      ova = ov.split(".", 4),
      nva = nv.split(".", 4);
    for (var i = 0; i < ova.length && i < nva.length; i++) {
      var so = ova[i], no = parseInt(so), sn = nva[i], nn = parseInt(sn);
      if (nn > no || sn.length > so.length) {
        return true;
      } else if (nn < no) {
        return false;
      }
    }
    if (nva.length > ova.length && 0 == nv.indexOf(ov)) {
      return true;
    }
  }

  // 下载wgt文件
  function downWgt() {
    plus.nativeUI.showWaiting("更新文件...");
    plus.downloader.createDownload(androidInfo.wgtURL, { filename: "_doc/update/" }, function (d, status) {
      if (status == 200) {
        console.log("下载wgt成功：" + d.filename);
        installWgt(d.filename); // 安装wgt包
      } else {
        console.log("下载wgt失败！");
      }
      plus.nativeUI.closeWaiting();
    }).start();
  }
  // 更新应用资源升级包
  function installWgt(path) {
    plus.nativeUI.showWaiting("安装文件...");
    plus.runtime.install(path, {}, function () {
      plus.nativeUI.closeWaiting();
      plus.nativeUI.alert("应用资源更新完成！", function () {
        plus.runtime.restart();
      });
    }, function (e) {
      plus.nativeUI.closeWaiting();
      plus.nativeUI.alert(JSON.stringify(e));
    });
  }
  /**
  * 创建下载任务 安卓
  */
  function createDownload() {
    var dtask = plus.downloader.createDownload(downloadUrl, { filename: '_doc/download/' }, function (d, status) {
      context.$vux.toast.text('准备安装', 'middle')
      // 下载完成
      if (status == 200) {
        plus.runtime.install(d.filename, {}, function () {
        }, function (DOMException) {
          // 弹出错误信息
          plus.nativeUI.alert('安装apk文件失败：'+DOMException.message);
        });
      } else {
        plus.nativeUI.alert("下载失败: " + status);
      }
    });
    dtask.start();
  }
  if (w.plus) {
    initUpdate();
  } else {
    document.addEventListener("plusready", initUpdate, false);
  }
}

export default  {
  updateVersion
}
