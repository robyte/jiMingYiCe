/**
 * @desc 这个js文件一般存放公用的工具函数，比如判断手机，用户名等
 */
import mui from '@/assets/js/mui.min.js'
import store from '../../vuexStore/store';
// 引入urlConfig 路径配置文件
const urlConfig = require("./url-config");
//toast弹窗公共配置项
const toastConfig = {
  text: "",
  type: "text",
  position: "top"
};

/**
 * @desc 支付方法
 * @param {curPayType} Number 选中支付的方式'0'代表支付宝支付  '1'代表微信支付
 * @param {ordercode} String 当前订单号
 * @param {ordercode} String 当前订单号
 */
function Alipay(context, curPayType, ordercode, _url) {
  if (!window.plus) {
    toastConfig.text = "plus未定义，请在app上查看";
    context.$vux.toast.show(toastConfig);
    return;
  }

  context.$vux.loading.show({
    text: "加载中..."
  });
  _url = _url || "/editUserInfo"; // 默认跳转到编辑用户信息页面
  if (curPayType) {
    //微信支付
    context.$fetch.post(urlConfig.payPrefix + "/payyice/gowxpay", { ordercode })
      .then(res => {
        console.log("微信支付", res.data);
        var self = plus.webview.currentWebview();
        var payView = plus.webview.create(res.data.url, "wxpay", {
          top: "1000px",
          bottom: "1000px",
          additionalHttpHeaders: { referer: urlConfig.payPrefix }
        });
        payView.hide()
        plus.webview.show(payView, 'pop-in', 100)
        // Webview窗口动画完成后的事件,必须有动画 否则无法监听
        payView.addEventListener('show', function (e) {
          var that = context
          var timer = setTimeout(() => {
            clearTimeout(timer)
            that.$vux.loading.hide();
            that.$vux.confirm.show({
              title: "温馨提示",
              content: "如果你已完成，请点击“已完成付款”",
              confirmText: "已完成付款",
              cancelText: "继续支付",
              hideOnBlur: true,
              onCancel: () => {
                Alipay(context, curPayType, ordercode, _url);
              },
              onConfirm: () => {
                that.$fetch
                  .post(urlConfig.fetchPrefix + "/app/jmyc/orderStatus", {
                    ordercode
                  })
                  .then(res => {
                    if (res.data.orders_pay_status == 2) {
                      console.log("微信支付完成", ordercode);
                      that.$router.replace(_url + "?ordercode=" + ordercode);
                    } else if (res.data.orders_pay_status == 1) {
                      that.$vux.confirm.show({
                        title: "温馨提示",
                        content: "支付失败",
                        // 组件除show外的属性
                        onCancel: () => { },
                        onConfirm: () => { }
                      });
                    }
                  });
              }
            });
          }, 2000);
        }, false);
      });
  } else {
    //支付宝
    context.$fetch
      .post(urlConfig.payPrefix + "/payyice/goalipay", {
        ordercode: ordercode
      }).then(res => {
        console.log('支付宝支付', res)
        window.localStorage.setItem('AliForm', JSON.stringify(res))
        let url =
          window.location.origin + window.location.pathname + "#/orderhome";
        var payView = plus.webview.create(url, "alipay", {
          top: "1000px",
          bottom: "1000px",
          additionalHttpHeaders: { referer: urlConfig.payPrefix }
        });
        payView.hide()
        plus.webview.show(payView, 'pop-in', 100)
        // Webview窗口动画完成后的事件
        payView.addEventListener('show', function (e) {
          var that = context
          var timer = setTimeout(() => {
            clearTimeout(timer)
            that.$vux.loading.hide();
            that.$vux.confirm.show({
              title: "温馨提示",
              content: "如果你已完成，请点击“已完成付款”",
              confirmText: "已完成付款",
              hideOnBlur: true,
              cancelText: "继续支付",
              onCancel: () => {
                Alipay(that, curPayType, ordercode, _url);
              },
              onConfirm: () => {
                that.$fetch
                  .post(urlConfig.fetchPrefix + "/app/jmyc/orderStatus", {
                    ordercode: that.ordercode
                  })
                  .then(res => {
                    if (res.data.orders_pay_status == 2) {
                      console.log("支付宝支付完成", ordercode);
                      that.$router.replace(_url + "?ordercode=" + ordercode);
                    } else if (res.data.orders_pay_status == 1) {
                      that.$vux.confirm.show({
                        title: "温馨提示",
                        content: "支付失败",
                        // 组件除show外的属性
                        onCancel: () => { },
                        onConfirm: () => { }
                      });
                    }
                  });
              }
            });
          }, 2000);
        }, false);
      });
  }
}

/**
 * @function 查看订单支付状态
 * @ordercode string 订单号
 */
function orderStatus(context, ordercode) {
  context.$fetch.post('/app/jmyc/orderStatus', {
    ordercode: ordercode
  }).then(res => {
    console.log(res)
  })
}

/**
 * @func
 * @desc  函数节流，多用于处理频繁调用函数的情况。n秒内频繁触发只触发第一次超过n秒继续再执行
 * @param {function} fn - 需要使用函数节流的被执行的函数，必传
 * @param {Number} interval 默认300ms - 需要间隔多少秒执行,可不传，默认300ms。可以不传
 */
function throttle(fn, interval) {
  var enterTime = 0; //触发的时间
  var gapTime = interval || 300; //间隔时间
  return function () {
    var that = context;
    var backTime = new Date();
    if (backTime - enterTime > gapTime) {
      //大于规定时间间隔时，再执行
      fn.call(that, arguments);
      enterTime = backTime;
    }
  };
}

/**
 * @func
 * @desc  函数防抖，用于多次触发，只执行 规定时间后再执行有且只有一次触发 的情况。便于优化性能
 * @param {function} fn - 需要使用函数防抖的被执行的函数。必传
 * @param {Number} interval - 多少毫秒之内多次触发，只执行最后一次，默认1000ms。可以不传
 */
function debounce(fn, interval) {
  var timer;
  var gapTime = interval || 1000;
  return function () {
    clearTimeout(timer);
    timer = null;
    var that = context;
    var arg = arguments;
    timer = setTimeout(function () {
      fn.call(that, arg);
    }, gapTime);
  };
}

/**
 * @func
 * @desc  检测手机号码
 * @param {Number} num - 需要检测的手机号
 * @returns {Boolean} - 检测错误则返回 false
 * @returns {String} - 检测正确则返回 去掉空格后的手机号
 */
function checkPhone(context, num) {
  var realNum = trimAll(num);
  if (realNum === "") {
    toastConfig.text = "手机号不能为空";
    context.$vux.toast.show(toastConfig);
    return false;
  }
  var reg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
  if (reg.test(realNum)) {
    return realNum;
  } else {
    toastConfig.text = "请输入正确的手机号";
    context.$vux.toast.show(toastConfig);
    return false;
  }
}

/**
 * @func
 * @desc  检测用户名
 * @param {String} name - 需要检测的用户名
 * @returns {Boolean} - 检测错误则返回 否则false
 * @returns {String} - 检测正确则返回 去掉空格后的用户名
 */
function checkUserName(context, name) {
  var realStr = trimAll(name);

  if (realStr === "") {
    toastConfig.text = "姓名不能为空";
    context.$vux.toast.show(toastConfig);
    return false;
  }
  if (realStr.length < 2) {
    toastConfig.text = "姓名最少为2个字";
    context.$vux.toast.show(toastConfig);
    return false;
  }
  if (realStr.length > 10) {
    toastConfig.text = "姓名最多为9个字符";
    context.$vux.toast.show(toastConfig);
    return false;
  }
  var reg = /^[\u0391-\uFFE5A-Za-z]{2,20}$/; //必须为汉字或者字母
  if (reg.test(realStr)) {
    return realStr;
  } else {
    toastConfig.text = "请输入正确的用户名";
    context.$vux.toast.show(toastConfig);
    return false;
  }
}

/**
 * @func
 * @desc  检测邮箱
 * @param {String} str - 需要检测的邮箱
 * @returns {Boolean} - 检测错误则返回  false
 * @returns {Boolean} - 检测正确则返回 去掉空格后的邮箱
 */
function checkEmail(context, str) {
  var realStr = trimAll(str);
  if (realStr === "") {
    toastConfig.text = "邮箱不能为空";
    context.$vux.toast.show(toastConfig);
    return false;
  }
  var reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
  if (reg.test(realStr)) {
    return realStr;
  } else {
    toastConfig.text = "请输入正确的邮箱";
    context.$vux.toast.show(toastConfig);
    return false;
  }
}

/**
 * @func
 * @desc  验证字符串是否为空
 * @param {String} str - 需要验证的字符串
 * @param {String} tip - 自定义的提示内容，注意内容不能太长。如果不传则使用默认的字符串
 * @returns {Boolean} - 如果为空则返回 false
 * @returns {String} - 如果不为空则返回原来的字符串
 */
function checkStrNull(context, str, tip) {
  var txt = tip ? tip : "内容不能为空"; //默认字符串为  内容不能为空
  if (!str) {
    toastConfig.text = txt;
    context.$vux.toast.show(toastConfig);
    return false;
  }
  return str;
}

/**
 * @func
 * @desc  去除字符串所有空格
 * @param {String} str - 需要去除空格的字符串
 * @returns {String} - 返回已去掉所有空格的字符串
 */
function trimAll(str) {
  return str.replace(/\s+/g, "");
}

/**
 * @func
 * @desc  截取0到end-1的字符串
 * @param {String} text - 字符串文本
 * @returns {String} - 如字符串长度小于end索引，返回字符串本身
 * @returns {String} - 否则返回从0截取到end的字符串+...
 */
function clipText(text, end) {
  if (text.length < end) {
    return text;
  } else {
    return text.substring(0, end) + "...";
  }
}

/**
 * @func
 * @desc  复制文本内容
 * @desc  注意，是target的点击事件里调用这个方法。然后target元素上必须，必须，必须要有 data-clipboard-text='xxx' 这个属性。
 * @param {String} context - 当前上下文
 * @param {String} target -  被复制目标的 class 或者 id 名
 */
function copyContent(context, target) {
  let targetObj = document.querySelector(target);
  let param = targetObj.getAttribute("data-clipboard-text");
  if (param == null) {
    console.error("data-clipboard-text属性不存在或值为null，请重新填写");
    return;
  }

  let clipboard = new context.$clipboard(target);

  clipboard.on("success", function () {
    toastConfig.text = "复制成功！";
    context.$vux.toast.show(toastConfig);
    clipboard.destroy(); //释放内存
  });
  clipboard.on("error", function () {
    toastConfig.text = "复制失败，请重试！";
    context.$vux.toast.show(toastConfig);
    clipboard.destroy(); //释放内存
  });
}

/**
 * @func
 * @desc  打开微信app
 */
function openWx() {
  window.location.href = "weixin://";
}

/**
 * @func
 * @desc  图片地址处理
 * @param  {String} url - 图片地址
 * @param {Boolean} isMaster - 是否是大师相关图片 默认false
 * @returns {String} - 返回处理后的图片地址
 */
function imgUrl(url, isMaster = false) {
  if (isMaster) {
    return urlConfig.masterImgPrefix + url;
  } else {
    return urlConfig.imgPrefix + url;
  }
}

/**
  @func
  @desc:等级
  @param: level - 等级
  @return: result - 等级对应的星星class属性值数组
*/
function starClass(level, on, half, gray) {
  let result = [];
  let score = Math.floor(level * 2) / 2;
  let hasDecimal = level % 1 !== 0;
  let integer = Math.floor(level);
  // 全亮
  for (let i = 0; i < integer; i++) {
    result.push(on);
  }
  // 半亮
  if (hasDecimal) {
    result.push(half);
  }
  // 灰色
  while (result.length < 5) {
    result.push(gray);
  }
  return result;
}

/**
 * @function 时间戳转换为日期格式
 * @param String timestamp 时间戳
 */
function timestampToTime(timestamp) {
  var date = new Date(timestamp * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
  var Y = date.getFullYear() + "-";
  var M =
    (date.getMonth() + 1 < 10
      ? "0" + (date.getMonth() + 1)
      : date.getMonth() + 1) + "-";
  var D = (date.getDate()<10? '0'+ date.getDate():date.getDate()) + " ";
  var h = (date.getHours()<10? '0'+ date.getHours():date.getHours()) + ":";
  var m = (date.getMinutes()<10? '0'+ date.getMinutes():date.getMinutes()) + ":";
  var s = date.getSeconds() <10 ? '0'+ date.getSeconds():date.getSeconds();
  return Y + M + D + h + m + s;
}

/**
 * @desc 设置沉浸式状态栏的背景色和颜色。
 * @param background 背景色，
 * @param color 字体颜色，只支持 dark 和 light 两种颜色
 */
function setBarStyle(background, color) {
  if (color == 'dark' || color == 'light') {
    if (window.plus) {
      console.log('设置状态栏')
      plus.navigator.setStatusBarBackground(background);//设置状态栏背景颜色
      plus.navigator.setStatusBarStyle(color);//设置状态栏字体颜色
    } else {
      let time = setTimeout(() => {
        setBarStyle(background, color)
        clearTimeout(time)
      }, 100)
    }
  } else {
    throw Error('设置状态栏第二个参数只支持“dark”，“light”两种颜色！')
  }
}
/**
 * @desc 将日期转换为选择日期插件所能接收的格式
 * @param date 日期 ，一种是接口返回的时间戳形式，一种是1996-01-05 10:00形式
 * @param noPoint 1时辰不清楚 0 清除
 * @paran isTimestamp 是否为时间戳形式，默认为true
 */
function handleBirthDate(date, noPoint, isTimestamp = true) {
  date = isTimestamp ? timestampToTime(date) : date
  let dateArr = date.split(" ");
  let dateYear = dateArr[0].replace(/-/g, "/");
  if (dateArr[1]) {
    let dateTime = dateArr[1].split(":");
    if (noPoint) {
      var dateHour = Number(dateTime[0]);
    } else {
      var dateHour = "-1";
    }
    var dateStr = dateYear + " " + dateHour;
  } else {
    var dateStr = dateYear + " " + "-1";
  }
  return dateStr;
}
/**
 * @desc 获取随机头像
 * @param gender 性别 1男 0女
 */
function getAvatar(gender) {
  let num = Math.floor(Math.random() * 5) + 1
  if (gender == 1) {
    return require(`../../../static/image/avatar/boy${num}.png`);
  } else {
    return require(`../../../static/image/avatar/girl${num}.png`);
  }
}
/**
 * 对象转数组
 */
function objToArr(obj) {
  var arr = []
  if (obj != {}) {
    for (var i in obj) {
      arr.push(obj[i])
    }
  }
  return arr
}
/**
 * @function 创建一个带导航栏的webview窗口
 * @param context this指向
 * @param webViewID String 窗口id名称
 * @param url String 链接地址
 * @param shareInfo  Object 分享信息 用于分享,是一个对象
{ href: 链接
  title: 标题
  content:  分享简介
  pictures: 分享图片
  thumbs: 分享缩略图
}
 */
function createNavigation(context,webViewID, url, shareInfo = false) {
  if (!window.plus) {
    mui.toast("plus未定义，请在app上查看");
    return;
  }
  // mui.toast("跳转链接中，请稍后");
  var viewConfig =
  {
    url: url,
    id: webViewID,
    styles: {                             // 窗口参数 参考5+规范中的WebviewStyle,也就是说WebviewStyle下的参数都可以在此设置
      titleNView: {                       // 窗口的标题栏控件
        autoBackButton: true,             // 新打开的窗口是否有返回键
        // titleText:"标题栏",                // 标题栏文字,当不设置此属性时，默认加载当前页面的标题，并自动更新页面的标题
        titleColor: "#000000",             // 字体颜色,颜色值格式为"#RRGGBB",默认值为"#000000"
        titleSize: "17px",                 // 字体大小,默认17px
        backgroundColor: "#F7F7F7",        // 控件背景颜色,颜色值格式为"#RRGGBB",默认值为"#F7F7F7"
        progress: {                        // 标题栏控件的进度条样式
          color: "#00FF00",                // 进度条颜色,默认值为"#00FF00"
          height: "2px"                    // 进度条高度,默认值为"2px"
        },
        splitLine: {                       // 标题栏控件的底部分割线，类似borderBottom
          color: "#CCCCCC",                // 分割线颜色,默认值为"#CCCCCC"
          height: "1px"                    // 分割线高度,默认值为"2px"
        },
        buttons: [
          // 关闭按钮
          { type: 'close', float: 'left', onclick: clickButton }
        ]
      }
    }
  }
  if (shareInfo) {
    // 分享按钮
    viewConfig.styles.titleNView.buttons.push({ type: 'share', float: 'right', onclick: shareButton })
  }
  var newView = mui.openWindow(viewConfig)
  store.commit('setWebView',true)
  // 监听后退事件
  mui.init({
    beforeback: function () {
      if (context.$store.state.openWebView) {
        //获得列表界面的webview
        var viewObj = plus.webview.getWebviewById(webViewID);
        //canBack查询窗口是否可退
        viewObj.canBack((event) => {
          var canBack = event.canBack;
          if (canBack) {//如可退，则返回上一页面
            viewObj.back();
          } else {//如不可退，则退出窗口
            plus.webview.close(webViewID)
            store.commit('setWebView', false)
          }
        })
        return false
      }
      return true
    }
  })
  // 关闭当前webview窗口
  function clickButton() {
    store.commit('setWebView', false)
    plus.webview.close(webViewID)
  }
  // 点击自定义分享按钮
  function shareButton() {
    if (!window.plus) {
      mui.toast("plus未定义，请在app上查看")
    }
    var shareOptions = {}
    // 监听plusready事件
    mui.plusReady(function () {
      // 扩展API加载完毕，现在可以正常调用扩展API
      plus.share.getServices(function success(data) {
        data.map((item, idx) => {
          shareOptions[item.id] = item
        })
      }, function error(e) {
        plus.nativeUI.toast("获取分享服务列表失败：" + e.message);
      });
    })
    // 分享链接点击事件
    var list = [
      {
        id: "weixin",
        imgurl: "static/image/public/msg_share_weixin.png",
        title: "微信",
        ex: 'WXSceneSession'
      },
      {
        id: "weixin",
        imgurl: "static/image/public/msg_share_friends.png",
        title: "朋友圈",
        ex: 'WXSceneTimeline'
      },
      {
        id: "qq",
        imgurl: "static/image/public/msg_share_qq.png",
        title: "QQ"
      },
      {
        id: "sinaweibo",
        imgurl: "static/image/public/msg_share_weibo.png",
        title: "微博"
      }
    ],
      bts = [
        { title: '微信' },
        { title: '朋友圈' },
        { title: 'qq' },
        { title: '微博' },
      ]
    plus.nativeUI.actionSheet({
      cancel: '取消',
      buttons: bts
    }, function clickButton(event) {
      var index = event.index; // 用户关闭时点击按钮的索引值 0表示点击的取消按钮，用户点击的button按钮的index从1开始
      if (index > 0) {
        var share_id = list[index - 1].id
        var share = shareOptions[share_id]
        if (share.authenticated) {
          shareMessage(share, list[index - 1].ex)
        } else {
            share.authorize(function () {
            shareMessage(share, list[index - 1].ex);
          }, function (e) {
            plus.nativeUI.toast("认证授权失败：" + e.code + " - " + e.message);
          });
        }
      }
    })
  }
  // 分享执行动作
  function shareMessage(share, ex) {
    var msg = {
      extra: {
        scene: ex
      },
      href: shareInfo.link,
      title: shareInfo.share_title || shareInfo.titlename,
      content:  shareInfo.share_des || '分享简介',
      pictures: shareInfo.share_img?[imgUrl(shareInfo.share_img)]: [imgUrl(shareInfo.imgurl)],
      thumbs: shareInfo.share_img?[imgUrl(shareInfo.share_img)]: [imgUrl(shareInfo.imgurl)]
    };
    if (share.id.indexOf('weibo') != -1) {
      msg.content += `；体验地址：${shareInfo.link}`;
      msg.pictures = ''
      msg.thumbs = ''
    }
    share.send(msg, function() {
      console.log("分享成功！ ");
    }, function(e) {
      let msg = e.code == -2 ? '已取消分享' : "分享失败";
      plus.nativeUI.toast(msg);
    });
  }
}
export default {
  throttle,
  debounce,
  checkPhone,
  checkUserName,
  trimAll,
  checkEmail,
  checkStrNull,
  clipText,
  copyContent,
  openWx,
  imgUrl,
  starClass,
  timestampToTime,
  Alipay,
  setBarStyle,
  handleBirthDate,
  getAvatar,
  objToArr,
  createNavigation
};
