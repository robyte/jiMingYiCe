/*
 * @Description: the configuration of url
 * @Author: lf
 * @Date: 2019-08-12 17:34:38
 * @LastEditTime: 2019-09-25 11:48:33
 * @LastEditors: Please set LastEditors
 */
const urlConfig = {
  masterImgPrefix: "https://ykdstatic.dd668.cn/",
  imgPrefix: 'http://ycadmin.dd668.cn/',
  // imgPrefix: 'http://ycadmintest.dd668.cn/', // 测试前缀图片
  payPrefix: 'https://pay.52dd.cn',  //支付 域名前缀
  // fetchPrefix:'https://yicetest.dd668.cn' // 请求接口的测试域名前缀
  fetchPrefix:'https://yice.dd668.cn' // 请求接口的域名前缀
}
module.exports = urlConfig
