/**
 * @desc axios封装
 * @desc get,post请求以及请求拦截，网络错误
 */
import axios from "axios";
import configUrl from './url-config.js';
import QS from "qs";
// 环境的切换
// if (process.env.NODE_ENV == "development") {
//   axios.defaults.baseURL = "https://apitest.52dd.com/";
// } else if (process.env.NODE_ENV == "production") {
  axios.defaults.baseURL = configUrl.fetchPrefix;
// }

// 请求超时时间,10s
axios.defaults.timeout = 10000;

// post请求头
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded;charset=UTF-8";

axios.defaults.withCredentials = false

/**
 * @desc get方法，对应get请求。外层再使用promise是为了可以连续多次使用get请求的情况
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
function get(url, params) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, {
        params: params
      })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data);
      });
  });
}
/**
 * @desc post方法，对应post请求 。外层再使用promise是为了可以连续多次使用get请求的情况
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
function post(url, params) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, QS.stringify(params))
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data);
      });
  });
}

export default {
  get,
  post
};
