import Vue from 'vue'
import Router from 'vue-router'
import store from'../vuexStore/store.js'

Vue.use(Router)

// 实现路由懒加载
function lazyLoad(filename){
  // return () => import(`@/page/${filename}/index.vue`);
  return require(`@/page/${filename}/index.vue`).default // 取消路由懒加载
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',  //首页
      component: lazyLoad('index'),
      meta: {
        keepAlive: true // 需要被缓存
      }
    },
    {
      path: '/index',
      name: 'indexToo',
      component: lazyLoad('index'),
      meta: {
        keepAlive: true // 需要被缓存
      }
    },
    {
      path: '/myCenter',
      name: 'z_myCenter',  //我的
      component: lazyLoad('z_myCenter'),
      meta: {
        keepAlive: true // 需要被缓存
      }
    },
    {
      path: '/confirmOrder',
      name: 'z_confirmOrder',  //确认订单页面
      component: lazyLoad('z_confirmOrder')
    },
    {
      path: '/infoDetail',
      name: 'h_infoDetail',  //咨询详情
      component: lazyLoad('h_infoDetail')
    },
    {
      path: '/msgCenter',
      name: 'h_msgCenter',  //消息中心
      component: lazyLoad('h_msgCenter'),
      meta: {
        keepAlive: true // 需要被缓存
      }
    },
    {
      path: '/askTeacher',
      name: 'h_askTeacher',//问大师
      component: lazyLoad('h_askTeacher'),
      meta: {
        keepAlive: true // 需要被缓存
      }
    },
    {
      path: '/teacherDetail',
      name: 'h_teacherDetail',//大师详情
      component: lazyLoad('h_teacherDetail')
    },
    {
      path:'/activityDetail',
      name:'h_activityDetail',
      component: lazyLoad('h_activityDetail')//活动详情
    },
    {
      path:'/allComment',
      name:'h_allComment',
      component: lazyLoad('h_allComment')//所有评论
    },
    {
      path: '/myOrder',
      name: 'z_myOrder',//我的订单
      component: lazyLoad('z_myOrder'),
      meta: {
        keepAlive: false // 需要被缓存
      }
    },
    {
      path: '/orderInfo',
      name: 'l_orderInfo',//订单详情页面
      component: lazyLoad('l_orderInfo')
    },
    {
      path: '/suggestionFeedback',
      name: 'l_suggestionFeedback', //意见反馈
      component: lazyLoad('l_suggestionFeedback')
    }, 
    {
      path: '/suggestionSuccess',
      name: 'l_suggestionSuccess', //意见反馈成功
      component: lazyLoad('l_suggestionSuccess')
    },
    {
      path: '/editUserInfo',
      name: 'z_editUserInfo', //填写用户信息
      component: lazyLoad('z_editUserInfo')
    },
    {
      path: '/share',
      name: 'l_share', //分享
      component: lazyLoad('l_share')
    },
    {
      path: '/submitSuccess',
      name: 'z_submitSuccess', //提交成功
      component: lazyLoad('z_submitSuccess')
    },
    {
      path: '/masterIn',
      name: 'l_masterIn', // 大师入驻
      component: lazyLoad('l_masterIn'),
      meta: {
        keepAlive: true // 需要被缓存
      },
    },
    {
      path: '/setting',
      name: 'l_setting',// 设置界面
      component: lazyLoad('l_setting')
    },
    {
      path: '/userPrivacy',
      name: 'l_userPrivacy',// 用户隐私界面
      component: lazyLoad('l_userPrivacy'),
    },
    {
      path: '/userAgreement',
      name: 'l_userAgreement',// 用户协议界面
      component: lazyLoad('l_userAgreement'),
    },
    {
      path: '/editPersonal',
      name: 'l_editPersonal',// 编辑个人信息
      component: lazyLoad('l_editPersonal'),
    },
    {
      path: '/brandOperationOne',
      name: 'f_brandOperationOne',  // 今日牌运首页
      component: lazyLoad('f_brandOperationOne')
    },
    {
      path: '/brandOperationTwo',
      name: 'f_brandOperationTwo',  // 今日牌运详情页
      component: lazyLoad('f_brandOperationTwo')
    },
    {
      path: '/luckOne',
      name: 'f_luckOne',  // 每日运势首页
      component: lazyLoad('f_luckOne')
    },
    {
      path: '/luckTwo',
      name: 'f_luckTwo',  // 每日运势详情页
      component: lazyLoad('f_luckTwo')
    },
    {
      path: '/guidelines',
      name: 'f_guidelines',  // 每日运势首页
      component: lazyLoad('f_guidelines')
    },
    {
      path: '/orderhome',
      name: 'orderhome',  // 支付宝支付处理页面
      component: lazyLoad('orderhome')
    },
    {
      path: '*',
      name: 'z_page404',  //404页面 ， 注意必须放在最底部
      component: lazyLoad('z_page404')
    }    
  ]
})
