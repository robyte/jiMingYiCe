import Vue from 'vue'
import App from './App'
import router from './router'
import mui from './assets/js/mui.min.js'
import fetch from './assets/js/request.js'
import store from'./vuexStore/store.js'
import clipboard from 'clipboard';

//注册复制粘贴方法到vue原型上
Vue.prototype.$clipboard = clipboard;

Vue.prototype.mui = mui;

import 'lib-flexible'

import  { ToastPlugin,AlertPlugin,ConfirmPlugin,LoadingPlugin   } from 'vux';
Vue.use(ToastPlugin)
Vue.use(AlertPlugin)
Vue.use(ConfirmPlugin)
Vue.use(LoadingPlugin)


Vue.config.productionTip = false;

// 将fetch挂载到vue的原型上,即可使用this.$fetch.get(url,param).then()
// 或者 this.$fetch.post(url,param).then()
Vue.prototype.$fetch = fetch; // 

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

router.beforeEach((to, from,next) => {
  store.commit('changePageLoading',true);//显示加载页面  
  next();
});

//解决跳转之后滚动位置不正确的问题
router.afterEach((to, from) => {
  if(to.meta.keepAlive) {
    window.scrollTo(0, 0);
  }
  store.commit('changePageLoading',false);//隐藏加载页面
});

// 日期格式指定化
Date.prototype.format = function(fmt){
  var o = {
    "M+" : this.getMonth()+1,                 //月份
    "d+" : this.getDate(),                    //日
    "h+" : this.getHours(),                   //小时
    "m+" : this.getMinutes(),                 //分
    "s+" : this.getSeconds(),                 //秒
    "q+" : Math.floor((this.getMonth()+3)/3), //季度
    "S"  : this.getMilliseconds()             //毫秒
  };

  if(/(y+)/.test(fmt)){
    fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  }
        
  for(var k in o){
    if(new RegExp("("+ k +")").test(fmt)){
      fmt = fmt.replace(
        RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));  
    }       
  }

  return fmt;
}